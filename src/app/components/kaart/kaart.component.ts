import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-kaart',
  templateUrl: './kaart.component.html',
  styleUrls: ['./kaart.component.scss']
})
export class KaartComponent implements OnInit {
    @Input() public cardName: string;
    @Input() public choiceColor: string;

    constructor(private sanitizer:DomSanitizer) {
    }

    ngOnInit() {
    }
    plusvier(){
        if(this.cardName == "WD4.png") return true;
        if(this.cardName == "WC.png") return true;
        return false;
    }
    reverse(){
        if(this.cardName == "RR.png") return true;
        if(this.cardName == "BR.png") return true;
        if(this.cardName == "YR.png") return true;
        if(this.cardName == "GR.png") return true;
        return false;
    }
    plus(){
        if(this.cardName == "RD2.png") return true;
        if(this.cardName == "BD2.png") return true;
        if(this.cardName == "YD2.png") return true;
        if(this.cardName == "GD2.png") return true;
        return false;
    }
    block(){
        if(this.cardName == "RS.png") return true;
        if(this.cardName == "BS.png") return true;
        if(this.cardName == "YS.png") return true;
        if(this.cardName == "GS.png") return true;
        return false;
    }
    getChoiceColor(){
        if(this.cardName.startsWith('W')){
            var c : string;
            if(this.choiceColor == "GREEN") c = "Green";
            else if(this.choiceColor == "RED") c = "Red";
            else if(this.choiceColor == "BLUE") c = "Blue";
            else if(this.choiceColor == "YELLOW") c = "Yellow";
            else return "";

            return this.sanitizer.bypassSecurityTrustStyle("1px solid " + c);
        }
        return "";
    }

}

