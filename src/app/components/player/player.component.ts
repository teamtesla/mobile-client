import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Player} from '../../model/player';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit, OnDestroy {
    public progress = 0;
    public imgUrl = '/assets/img/unknown-player.png';
    private interval;
    private plusCards: number = null;
    @Input() public speler: Player;
    @Input() public beurt: boolean;
    @Input() public events: Observable<number>;

    private eventsSubscription: any;

    constructor() { }

    ngOnDestroy(): void {
        this.eventsSubscription.unsubscribe();
    }

    // TODO vervang naam door user.name
    ngOnInit() {
        // TODO check profile picture en indien deze bestaat vervang imgUrl

        var _this = this;
        this.eventsSubscription = this.events.subscribe((x) => {
            _this.plusCards = x;
            setTimeout(function(){
                _this.plusCards = null;
            },2000)
        })
    }

    ngOnChanges() {
        if (this.progress === 100 || this.progress === 0) {
            if (this.beurt) {
                this.startTimer();
            }
        }
    }

    startTimer() {
        this.progress = 0;
        this.interval = setInterval(() => {
            this.progress++;
            console.log(this.progress);
            if (this.progress === 100) {
                clearInterval(this.interval);
                this.progress = 0;
                this.beurt = false;
            }
        }, 100);
    }

    public startBeurt() {
        this.startTimer();
    }
}
