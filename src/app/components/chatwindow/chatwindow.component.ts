import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ChatMessage} from '../../model/chat-message';
import {Lobby} from '../../model/lobby';
import {NavParams} from '@ionic/angular';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';
import {LobbyService} from '../../services/lobby.service';
import {User} from '../../model/user';

@Component({
    selector: 'app-chatwindow',
    templateUrl: './chatwindow.component.html',
    styleUrls: ['./chatwindow.component.scss']
})
export class ChatwindowComponent implements OnInit {

    chatlog: ChatMessage[] = new Array();
    user: string;
    public newMessage: ChatMessage = new ChatMessage('', null, null);
    private currentLobby: Lobby = null;

    @ViewChild('chatInput') messageField: ElementRef;

    constructor(private dispatcher: WebsocketDispatcherService, private lobbyService: LobbyService) {
        this.currentLobby = lobbyService.getLastCurrentLobby();
        this.receiveMessages();
    }

    sendMessage() {
        console.log(this.newMessage.message);
        if (this.newMessage.message !== '') {
            this.newMessage.lobby = this.currentLobby;
            this.dispatcher.sendMessage('chat/message', this.newMessage);
            this.newMessage.sender = new User('You', null, null);
        }
        this.receiveMessages();
    }

    receiveMessage() {
        this.dispatcher.subscribeOnPersonalTopic('chat/message', (x) => {
            this.newMessage = JSON.parse(x);
        });
    }

    receiveMessages() {
        this.dispatcher.subscribeOnPersonalTopic('chat/history', (x) => {
            console.log('receiving...');
            console.log(x);
            this.chatlog = JSON.parse(x);
        });
        this.dispatcher.sendMessage('chat/history', {});
    }

    ngOnInit() {
    }
}