import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Invite} from '../../model/invite';
import {User} from '../../model/user';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';
import { LobbyService } from '../../services/lobby.service';

@Component({
    selector: 'app-invite',
    templateUrl: './invite.component.html',
    styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit {
    inviteconstruct:Invite = new Invite();
    public nameInput: string;
    friends : User[] = new Array();
    chipUsers : string[] = new Array();
    selectedFriends : string[] = new Array();
    public invitationList: string[] = [];
    @Output() onInviteClose = new EventEmitter<boolean>();
    @Output() onInvite = new EventEmitter<boolean>();
    constructor(private dispatcher: WebsocketDispatcherService, private lobbyService : LobbyService) {
        this.mockData();
    }

    ngOnInit() {
    }

    public addToList() {
        const name = this.nameInput;
        this.invitationList.push(name);
    }

    close(){
        this.onInviteClose.emit(true);
    }
    invite(){
        this.dispatcher.sendMessage("invitation/send",this.getInviteObject())
        this.onInvite.emit(true);
    }
    chipRemove(u){
        const index = this.chipUsers.indexOf(u);

        if (index >= 0) {
            this.chipUsers.splice(index, 1);
        }
    }
    /*chipAdd(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        // Add our fruit
        if ((value || '').trim()) {
            this.chipUsers.push(value.trim());
        }

        // Reset the input value
        if (input) {
            input.value = '';
        }
    }*/
    mockData(){
        var u1 : User = new User("Toon","","");
        var u2 : User = new User("Hannah","","");
        var u3 : User = new User("Potatoe","","");

        var u4 : User = new User("Toon","","");
        var u5 : User = new User("Hannah","","");
        var u6 : User = new User("Potatoe","","");

        var u7 : User = new User("Toon","","");
        var u8 : User = new User("Hannah","","");
        var u9 : User = new User("Potatoe","","");
        this.friends.push(u1,u2,u3,u4,u5,u6,u7,u8,u9);
    }
    getInviteString(){
        this.inviteconstruct.receivernames = new Array();
        this.inviteconstruct.receivernames.push(this.chipUsers.toString());
        this.inviteconstruct.receivernames.push(this.selectedFriends.toString());
        this.inviteconstruct.lobby = this.lobbyService.getLastCurrentLobby().lobbyId;
        return JSON.stringify(this.inviteconstruct);
    }
    getInviteObject() : Invite{
        this.inviteconstruct.receivernames = this.invitationList;
        this.inviteconstruct.lobby = this.lobbyService.getLastCurrentLobby().lobbyId;
        return this.inviteconstruct;
    }
    checkboxChange(f, event){
        if(event.checked)
            this.selectedFriends.push(f.username);

        else{
            const index = this.selectedFriends.indexOf(f.username);

            if (index >= 0) {
                this.selectedFriends.splice(index, 1);
            }
        }
    }
}

