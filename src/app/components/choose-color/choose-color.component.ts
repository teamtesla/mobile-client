import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-choose-color',
  templateUrl: './choose-color.component.html',
  styleUrls: ['./choose-color.component.scss']
})
export class ChooseColorComponent implements OnInit {
    private color: string;
    public activated = false;
    @Output() chosenColor = new EventEmitter<string>();

    constructor() { }

    ngOnInit() {
    }

    public choose(color: string) {
       this.chosenColor.emit(color);
    }
}
