import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {NotificationService, NotificationSort} from '../../services/notification.service';
import {LobbyChange} from '../lobbies/lobbies.page';
import {LogService, LogType} from '../../services/log.service';
import {Observable, Subject, Subscription} from 'rxjs';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';
import {Lobby} from '../../model/lobby';
import {Game} from '../../model/game';
import {User} from '../../model/user';
import {UserStateService} from '../../services/user-state.service';
import {Router} from '@angular/router';
import {LobbyService} from '../../services/lobby.service';
import {Promise} from 'q';

@Component({
    selector: 'app-lobby',
    templateUrl: './lobby.page.html',
    styleUrls: ['./lobby.page.scss'],
})
export class LobbyPage implements OnInit, OnDestroy {
    public inviteDialogOpen = false;
    public currentLobby: Lobby = null;
    public inviting = false;
    private isHost = false;
    public games: Game[];
    public startGame: Subject<boolean> = new Subject<boolean>();
    private players: User[];
    private created: string;
    private subscriptions: Subscription[] = new Array();

    constructor(private dispatcher: WebsocketDispatcherService, private lobbyService: LobbyService, private router: Router, private stateService: UserStateService, private logger: LogService, private notifier: NotificationService) {
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    ngOnInit() {
        this.currentLobby = this.lobbyService.getLastCurrentLobby();
        this.getGames();
        /*
            this.currentLobby = new Lobby("Jan's lobby");
            this.currentLobby.playerCount = 4;
            this.currentLobby.name ="test";
            this.currentLobby.unoUsers.push(new User("1","Juan","123456","jvah@hotmail.com"));
            console.log("[CURRENT LOBBY STRING]" + JSON.stringify(this.currentLobby));
        */

        console.log('LOBBY COMPONENT CONSTRUCTOR');
        const s = this.lobbyService.currentLobbyObservable().subscribe((x) => {
            if (x.deleted) {
                alert('Your lobby has been deleted.');
                this.router.navigate(['/lobbies']);

            }
            this.logger.log('CURRENT LOBBY CHANGE', 3, LogType.log, this);
            this.currentLobby = x;

            this.logger.log(JSON.stringify(x), 3, LogType.log, this);


        });
        this.subscriptions.push(s);


        // CHECK USER STATUSES
        if (this.currentLobby != null && this.currentLobby.unoUsers != null)
            this.currentLobby.unoUsers.forEach(u => {
                u.alive = this.stateService.isAlive(u);
                this.logger.log('USER STATE CHECK: ' + JSON.stringify(u) + ': ' + u.alive, 1, LogType.log, this);
            });
        const s2 = this.stateService.userStateChangerObservable().subscribe(user => {
            if (this.currentLobby != null && this.currentLobby.unoUsers != null) {
                this.currentLobby.unoUsers.forEach(u => {
                    if (user.unoUserId === u.unoUserId) u.alive = user.alive;
                    this.logger.log('USER STATE CHECK: ' + JSON.stringify(u) + ': ' + u.alive, 1, LogType.log, this);

                });
            }
        });
        this.subscriptions.push(s2);
    }

    removeLobby() {
        this.dispatcher.sendMessage('lobby/remove', new LobbyChange(this.currentLobby.lobbyId));
        this.router.navigate(['/lobbies']);
    }

    leaveLobby() {
        this.dispatcher.sendMessage('lobby/leave', new LobbyChange(this.currentLobby.lobbyId));
        this.router.navigate(['/lobbies']);
        this.dispatcher.subscribeOnPersonalTopic('game/list', (x) => {
            this.games = JSON.parse(x);
        });
        this.dispatcher.sendMessage('game/list', {});
    }

    getGames() {
        this.logger.log('GAME LIST 1:', 3, LogType.log, this);
        this.dispatcher.subscribeOnPersonalTopic('game/list', (x) => {
            this.games = JSON.parse(x);
            this.startGame.next(true);
        });
        this.dispatcher.sendMessage('game/list', {});
    }

    addGame() {
        this.dispatcher.sendMessage('game/create', {o: this.currentLobby.lobbyId});
        this.created = 'Game created';
        this.getGames();
        this.startGame.subscribe(() => { this.router.navigate(['/game']); });
    }

    back() {
        this.router.navigate(['/lobbies']);
    }

    playerCountChange() {
        this.dispatcher.sendMessage('lobby/update', this.currentLobby);
    }

    lobbyNameChange() {
        this.dispatcher.sendMessage('lobby/update', this.currentLobby);
    }

    onInvite() {
        this.inviteDialogOpen = false;
        this.notifier.showNotification('Invite sent', NotificationSort.notify);
    }

    makePrivate() {
        this.dispatcher.sendMessage('lobby/update', this.currentLobby);
    }

    public openInvites() {
        this.inviting = true;
    }

    public closeInvites() {
        this.inviting = false;
    }

}
