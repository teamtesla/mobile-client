import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../model/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
    public error: String;
    public success = false;
    public user: User = new User('', '', '');
    public passwordVerification: String = '';

    constructor(private userService: UserService) {
    }

  ngOnInit() {
  }

    onSubmit() {
        this.error = '';
        this.addUser(this.user);
    }

    addUser(user: User) {
        this.userService.addUser(user).subscribe((result) => {
            if (result) {
                this.success = true;
                this.error = '';
            } else {
                this.error = this.userService.getErrorMessage;
            }
        }, (err) => {
            this.error = err;
        });
    }

}
