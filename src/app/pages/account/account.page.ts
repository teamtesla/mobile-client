import {Component, OnInit} from '@angular/core';
import {User} from '../../model/user';
import {Lobby} from '../../model/lobby';
import {UserStatistics} from '../../model/user-statistics';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';
import {Router} from '@angular/router';
import {LobbyService} from '../../services/lobby.service';
import {LobbyChange} from '../lobbies/lobbies.page';

@Component({
    selector: 'app-account',
    templateUrl: './account.page.html',
    styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
    public currentUser: User;
    public lobbies: Lobby[];
    private currentLobby: Lobby = null;
    private statistics: UserStatistics = null;

    constructor(private dispatcher: WebsocketDispatcherService, private router: Router, private lobbyService: LobbyService) {
        this.currentLobby = lobbyService.getLastCurrentLobby();
        this.getUser();
        this.getPersonalLobbies();
        this.getStats();
    }

    ngOnInit() {
    }

    getUser() {
        this.dispatcher.subscribeOnPersonalTopic('user/info', (x) => {
            this.currentUser = JSON.parse(x);
            console.log(x);
        });
        this.dispatcher.sendMessage('user/info', {});
    }

    getPersonalLobbies() {
        this.lobbies = [];
        this.dispatcher.subscribeOnPersonalTopic('lobby/personal', (x) => {
            this.lobbies = JSON.parse(x);
            console.log(x);
        });
        this.dispatcher.sendMessage('lobby/personal', {});
    }

    joinLobby(lobby: Lobby) {
        if (this.currentLobby !== null && this.currentLobby.lobbyId === lobby.lobbyId) {
            this.router.navigate(['/lobby']);
        } else {
            this.lobbyService.lastCurrentLobby = null;
            this.dispatcher.sendMessage('lobby/change', new LobbyChange(lobby.lobbyId));
            this.router.navigate(['/lobby']);
        }
    }

    getStats() {
        this.dispatcher.subscribeOnPersonalTopic('statistics/getpersonal', (x) => {
            this.statistics = JSON.parse(x);
        });
        this.dispatcher.sendMessage('statistics/getpersonal', {});
    }
}
