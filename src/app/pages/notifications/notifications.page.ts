import { Component, OnInit } from '@angular/core';
import {LobbyService} from '../../services/lobby.service';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

    notificationItems : NotificationItem[] = new Array();
    constructor(
        private lobbyService : LobbyService,
        private dispatcher : WebsocketDispatcherService,
        private router : Router) { }

    ngOnInit() {
        this.getLobbyInvites();
    }
    getLobbyInvites(){
        this.iterateLobbyInvites(this);
        var _this = this;
        this.lobbyService.lobbyInvites.asObservable().subscribe((x) => {
            _this.iterateLobbyInvites(_this);
        });
    }

    doRefresh(event) {
        setTimeout(() => {
            this.iterateLobbyInvites(this);
            event.target.complete();
        }, 2000);
    }

    private iterateLobbyInvites(_this){
        _this.notificationItems = new Array();
        _this.lobbyService.lastLobbyInvites.forEach((x)=>{
            var notitem : NotificationItem = new NotificationItem();
            notitem.setSort(NotificationItemSort.lobbyInvite);
            notitem.invite = x;
            notitem.message = x.sender.username + " invites you to join his/her lobby";
            _this.notificationItems.push(notitem);
        })
    }
    accept(not : NotificationItem){
        if(not.getSort() == NotificationItemSort.lobbyInvite){
            this.dispatcher.sendMessage("invitation/remove",not.invite);
            this.joinLobby(not.invite.lobby)
        }
    }
    decline(not : NotificationItem){
        if(not.getSort() == NotificationItemSort.lobbyInvite){
            this.dispatcher.sendMessage("invitation/remove",not.invite);
        }
    }
    joinLobby(lobby:number){

        this.lobbyService.lastCurrentLobby = null;
        this.dispatcher.sendMessage("lobby/change",new LobbyChange(lobby));
        this.router.navigate(["/lobby"]);

    }
}
class LobbyChange{
    id : number;
    constructor(n:number ){
        this.id = n;
    }
}


class NotificationItem{
    message:string;
    private sort:NotificationItemSort;
    invite:any;
    color:string;

    setSort(sort :NotificationItemSort){
        this.sort = sort;
        if(this.sort == NotificationItemSort.friendInvite) this.color = "#a5daba";
        if(this.sort == NotificationItemSort.lobbyInvite) this.color = "#ffbb56";

    }
    getSort() : NotificationItemSort{
        return this.sort;
    }

}
enum NotificationItemSort{
    friendInvite,
    lobbyInvite
}