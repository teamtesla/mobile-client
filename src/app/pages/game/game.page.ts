import {Component, OnDestroy, OnInit} from '@angular/core';
import {Card, Color} from '../../model/card';
import {ChooseColorComponent} from '../../components/choose-color/choose-color.component';
import {Player} from '../../model/player';
import {Subject} from 'rxjs';
import {UserService} from '../../services/user.service';
import {GameService} from '../../services/game.service';
import {LogService, LogType} from '../../services/log.service';
import {DomSanitizer} from '@angular/platform-browser';
import {ModalController, ToastController} from '@ionic/angular';
import {LoginPage} from '../login/login.page';
import {KaartComponent} from '../../components/kaart/kaart.component';
import {LobbiesPage} from '../lobbies/lobbies.page';
import {ChatwindowComponent} from '../../components/chatwindow/chatwindow.component';


@Component({
    selector: 'app-game',
    templateUrl: './game.page.html',
    styleUrls: ['./game.page.scss'],
})
export class GamePage implements OnInit, OnDestroy {
    private gameId: number;
    public loaded = false;
    public currentCard: Card;
    public players: Player[] = [null, new Player(null, null, null, null, null), null, null, null, null, null, null, null];
    private deck: Card[] = [];
    public player: Player;
    private uno: boolean;
    private first = true;
    public colorActivatedCard;
    public chatActivated = false;
    private cardplusSubject: Subject<number>[] = [new Subject<number>(), new Subject<number>(), new Subject<number>(), new Subject<number>(), new Subject<number>(), new Subject<number>(), new Subject<number>(), new Subject<number>()];

    constructor(
        private userService: UserService,
        private gameService: GameService,
        private logger: LogService,
        private sanitizer: DomSanitizer,
        private toastController: ToastController) {
    }

    ngOnInit() {
        this.gameService.getGameObservable().subscribe((x) => {
            this.assignCurrentCard(x.topCard.color, x.topCard.value, x.topCard.cardId, x.color, this);

            this.logger.log('TURN RECEIVED ' + JSON.stringify(x), 2, LogType.special, this);

            const game = x;
            this.gameId = game.gameId;
            this.uno = false;

            const tempPlayerDeck = x.players.find(playerX => playerX.cards != null).cards.map(card => new Card(card.color, card.value, card.cardId));

            this.currentCard.ranrot = Math.random() * 13;
            this.currentCard.ranX = Math.random() * 30 - 15;
            this.currentCard.ranY = Math.random() * 30 - 15;

            // check if received top card is new card, if so add card to deck
            if (this.deck.length === 0) { this.deck.push(this.currentCard); }
            else {
                this.logger.log('DECK SIZE:' + this.deck.length, 1, LogType.special, this);
            }
            if (this.currentCard.cardId !== this.deck[this.deck.length - 1].cardId) {
                this.deck.push(this.currentCard);
            } else {
                this.logger.log('SAME CARD' + JSON.stringify(this.currentCard) + ' | ' + JSON.stringify(this.deck), 1, LogType.special, this);
            }

            /// if (x.players.find(playerX => playerX.cards != null).cards.length != tempPlayerDeck.length || _this.first == true) {
            x.players.find(playerX => playerX.cards != null).cards = tempPlayerDeck;
            this.first = false;


            // Calculate pluscards
            const pluscards: UserPlusCard[] = [];
            for (let index = 0; index < this.players.length; index++) {
                const elementA: Player = this.players[index];
                if (elementA != null) {
                    const elementB: Player = x.players[index];


                    const result = elementB.amount - elementA.amount;
                    const pc: UserPlusCard = {cardCount: result, userId: index};
                    if (result > 0) {
                        pluscards.push(pc);
                    }
                }
            }
            pluscards.forEach(pc => {
                this.cardplusSubject[pc.userId].next(pc.cardCount);
            });


            this.players = x.players;
            this.player = x.players.find(playerX => playerX.cards != null);
            if (this.player.turn) {
                this.showToast('It\'s your turn to play now!');
            }
            this.players = x.players;
            this.loaded = true;
            if (this.players.find(x => x.amount === 0)) {
                this.showToast(this.players.find(x => x.amount === 0).username + ' won the game!');
                this.players.map(x => x.turn = false);
            }
        });
        this.gameService.initializeGame();
    }

    ngOnDestroy() {
        this.gameService.destroyGame();
    }

    public openChat() {
        this.chatActivated = true;
        console.log('yeahyeah');
    }

    public closeChat() {
        this.chatActivated = false;
    }

    public clickUno() {
        if (this.player.cards.length === 2) {
            this.uno = true;
            this.showToast('You called UNO!');
        } else {
            this.showToast('You can only say UNO when you have 2 cards left!');
        }
    }

    private async showToast(message: string) {
        const toast = await this.toastController.create({
            message: message,
            duration: 2000
        });
        toast.present();
    }

    private assignCurrentCard(color: string, value: string, id: number, choice: string, _this) {
        _this.currentCard = new Card(color, value, id);
        _this.currentCard.choiceColor = choice;
    }

    private assignTurnToUser(userId: number) {
        this.players.find(user => user.userId === userId).turn = true;
    }

    onCardClicked(card: Card) {
        if (this.player.turn) {
            if (this.isClickedCardPossible(card) && !this.isColorNeeded(card)) {
                this.currentCard = card;
                this.deck.push(card);

                // remove played card from hand
                const index = this.player.cards.indexOf(card);
                if (index > -1) {
                    this.player.cards.splice(index, 1);
                }


                this.gameService.sendGameData(this.gameId, card.cardId, this.player.userId, this.uno);
            } else if (this.isColorNeeded(card)) {
                this.sendColorCard(card);
            }
        } else {
            this.showToast('It\'s currently not your turn!');
        }
    }

    takeCard() {
        if (this.player.turn) {
            this.gameService.sendGameData(this.gameId, 999, this.player.userId, this.uno);
            this.showToast('You took a card from the deck!');
        } else {
            this.showToast('You can only take a card when it\'s your turn!');
        }
    }

    sendColorCard(card: Card) {
        this.colorActivatedCard = card;
    }

    chooseColor(color) {
        this.gameService.sendGameDataWithColor(this.gameId, this.colorActivatedCard.cardId, this.player.userId, false, color);
        this.colorActivatedCard = null;
    }

    isClickedCardPossible(card: Card): boolean {
        const lastCard = this.deck[this.deck.length - 1];
        if ((card.color as Color === Color.Wild) || (card.color === lastCard.color) || card.number === lastCard.number) {
            return true;
        } else {
            return Color.Wild === lastCard.color as Color;
        }
    }

    isColorNeeded(card: Card): boolean {
        return card.image === 'WD4' || card.image === 'WC';
    }

    getSafeTransform(card: Card) {
        return this.sanitizer.bypassSecurityTrustStyle('rotate(' + card.ranrot + 'deg) translate(' + card.ranX + 'px,' + card.ranY + 'px)');
    }
}

class UserPlusCard {
    userId: number;
    cardCount: number;
}
