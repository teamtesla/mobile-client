import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GamePage } from './game.page';
import {PlayerComponent} from '../../components/player/player.component';
import {HandComponent} from '../../components/hand/hand.component';
import {KaartComponent} from '../../components/kaart/kaart.component';
import {ChooseColorComponent} from '../../components/choose-color/choose-color.component';
import {ChatwindowComponent} from '../../components/chatwindow/chatwindow.component';

const routes: Routes = [
  {
    path: '',
    component: GamePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GamePage, PlayerComponent, HandComponent, KaartComponent, ChooseColorComponent, ChatwindowComponent]
})
export class GamePageModule {}
