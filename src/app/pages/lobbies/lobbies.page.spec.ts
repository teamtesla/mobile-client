import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbiesPage } from './lobbies.page';

describe('LobbiesPage', () => {
  let component: LobbiesPage;
  let fixture: ComponentFixture<LobbiesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LobbiesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbiesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
