import {AfterViewChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import {Lobby} from '../../model/lobby';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';
import {LogService, LogType} from '../../services/log.service';
import {Router} from '@angular/router';
import {LobbyService} from '../../services/lobby.service';
import {delay} from 'rxjs/operators';
import {UserService} from '../../services/user.service';

@Component({
    selector: 'app-lobbies',
    templateUrl: './lobbies.page.html',
    styleUrls: ['./lobbies.page.scss'],
})
export class LobbiesPage implements OnInit {

    public lobbies: Lobby[];
    private currentLobby: Lobby = null;

    constructor(private dispatcher: WebsocketDispatcherService, private router: Router, private lobbyService: LobbyService, private logger: LogService, private userService: UserService) {
        this.currentLobby = this.lobbyService.getLastCurrentLobby();

        const _this = this;
        this.lobbyService.currentLobbyObservable().subscribe(function (x) {

            _this.logger.log('CURRENT LOBBY CHANGE', 3, LogType.log, _this);
            _this.currentLobby = x;

            _this.logger.log(JSON.stringify(x), 4, LogType.log, _this);

        });
        this.dispatcher.sendMessage("lobby/list",new Object());
    }
    ngOnInit() {
        this.lobbies = new Array();
        this.lobbies = this.lobbyService.getLastLobbyList();
        this.setColors(this);
        var _this = this;
        this.lobbyService.lobbyListObservable().subscribe(function(x){
            _this.logger.log("lobby list updated",1,LogType.log,_this);
            _this.lobbies = x;
            _this.logger.log(JSON.stringify(x),4,LogType.log,_this);
            _this.setColors(_this);
        });
        this.dispatcher.sendMessage('lobby/list', {});
        console.log('HEYHEY' + this.userService.getUuid());
    }

    setColors(_this){
        var count = 0;
        _this.lobbies.forEach(l=>{
            if(count == 0) l.color = "#ED1C24";
            if(count == 1) l.color = "#FFDE00";
            if(count == 2) l.color = "#0095DA";
            if(count == 3) l.color = "#00A651";
            count++;
            if(count == 4) count = 0;
        })
    }

    public addLobby() {
        this.dispatcher.sendMessage('lobby/create', new LobbyCreate('Jan\'s lobby'));
        this.router.navigate(['/lobby']);
    }

    public joinLobby(lobby: Lobby) {
        if (this.currentLobby != null && this.currentLobby.lobbyId === lobby.lobbyId) {
            this.router.navigate(['/lobby']);
        } else {
            this.lobbyService.lastCurrentLobby = null;
            this.dispatcher.sendMessage('lobby/change', new LobbyChange(lobby.lobbyId));
            this.router.navigate(['/lobby']);
        }
    }
}

export class LobbyCreate {
    name: String;

    constructor(n: string) {
        this.name = n;
    }
}

export class LobbyChange {
    id: number;

    constructor(n: number) {
        this.id = n;
    }
}
