import {Component, OnInit} from '@angular/core';
import {Lobby} from '../../model/lobby';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';
import {Router} from '@angular/router';
import {LobbyService} from '../../services/lobby.service';
import {LogService} from '../../services/log.service';

@Component({
    selector: 'app-personallobbies',
    templateUrl: './personallobbies.page.html',
    styleUrls: ['./personallobbies.page.scss'],
})
export class PersonallobbiesPage implements OnInit {
    public lobbies: Lobby[];
    private currentLobby: Lobby = null;

    constructor(private dispatcher: WebsocketDispatcherService,
                private router: Router,
                private lobbyService: LobbyService,
                private logger: LogService) {
    }

    ngOnInit() {
        this.lobbies = [];
        const topic = 'lobby/personal';
        this.dispatcher.subscribeOnPersonalTopic(topic, (x) => {
            this.lobbies = JSON.parse(x);
        });
        this.dispatcher.sendMessage(topic, {});
    }

}
