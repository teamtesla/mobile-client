import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonallobbiesPage } from './personallobbies.page';

describe('PersonallobbiesPage', () => {
  let component: PersonallobbiesPage;
  let fixture: ComponentFixture<PersonallobbiesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonallobbiesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonallobbiesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
