import {Component, OnInit, ViewChild} from '@angular/core';
import {WebsocketDispatcherService} from '../../services/websocket-dispatcher.service';
import {UserStatistics} from '../../model/user-statistics';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.page.html',
  styleUrls: ['./statistics.page.scss'],
})
export class StatisticsPage implements OnInit {
    userStats: UserStatistics[] = new Array();
    showit: number;

    constructor(private dispatcher: WebsocketDispatcherService) {
      this.getStats();
      this.showit = 1;
  }

  ngOnInit() {
  }
    getStats() {
        this.dispatcher.subscribeOnPersonalTopic("statistics/users", (x) => {
            this.userStats = JSON.parse(x);
        });
        this.dispatcher.sendMessage("statistics/users", {});
    }
    setMostWins() {
        this.showit = 1;
    }
    setMostPoints() {
        this.showit = 2;
    }
    setMostGames() {
        this.showit = 3;
    }

    getSortedStats(sortMode: number): UserStatistics[] {
        let sortedStats: UserStatistics[];
        switch (sortMode) {
            case 0:
                sortedStats = this.userStats.sort(function (a, b) {
                    return b.gamesWon - a.gamesWon;
                });
                break;
            case 1:
                sortedStats = this.userStats.sort(function (a, b) {
                    return b.pointsEarned - a.pointsEarned;
                });
                break;
            case 2:
                sortedStats = this.userStats.sort(function (a, b) {
                    return b.gamesPlayed - a.gamesPlayed;
                });
                break;
        }
        return sortedStats;
    }
}
