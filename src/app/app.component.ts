import {AfterViewInit, Component} from '@angular/core';

import {Platform, MenuController, ToastController} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {NotificationService, NotificationSort} from './services/notification.service';
import {WebsocketDispatcherService} from './services/websocket-dispatcher.service';
import {UserStateService} from './services/user-state.service';
import {LobbyService} from './services/lobby.service';
import {UserService} from './services/user.service';
import {GameService} from './services/game.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent implements AfterViewInit {

    title = 'Uno';
    notification = 'test';
    notifications: NotificationShowItem[] = new Array();
    constructor(
        private userService: UserService,
        private lobbyService: LobbyService,
        private userStateService: UserStateService,
        private dispatcher: WebsocketDispatcherService,
        private notifier: NotificationService,
        private gameService: GameService,
        private toastController: ToastController) {

    }
    ngAfterViewInit(): void {
        this.doSubscriptions();
        this.userService.checkLoggedIn();
    }
    logout() {
        this.userService.doLogout();
    }

    isLoggedin(): boolean {
        return this.userService.isLoggedin();
    }

    async showToast(message: string) {
        const toast = await this.toastController.create({
            message: message,
            duration: 2000
        }).then(result => result);
        toast.present();
    }

    doSubscriptions() {
        this.lobbyService.Start();
        this.lobbyService.StartInvitationListeners();
        this.gameService.start();
        this.userStateService.start();
        var _this = this;
        this.notifier.getNotifier().subscribe((nbar) => {
            var nsi = new NotificationShowItem();
            if (nbar.sort == NotificationSort.notify) nsi.color = "rgb(107, 171, 109)";
            if (nbar.sort == NotificationSort.warn) nsi.color = "orange";
            if (nbar.sort == NotificationSort.error) nsi.color = "red";
            nsi.message = nbar.message;
            this.showToast(nsi.message);
        });
    }
    removeNotification(x, _this) {
        const index = _this.notifications.indexOf(x);

        if (index >= 0) {
            _this.notifications.splice(index, 1);
        }
    }
    getNotificationNumber() : number {
        return this.lobbyService.lastLobbyInvites.length;
    }
}
class NotificationShowItem {
    color: string;
    message: string;
}
