import { Injectable } from '@angular/core';
import {WebsocketDispatcherService} from './websocket-dispatcher.service';
import {Game} from '../model/game';
import {Observable, Subject} from 'rxjs';
import {Turn} from '../model/turn';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private game: Subject<Game> = new Subject();

  constructor(private dispatcher: WebsocketDispatcherService) { }

  public start() {
    this.dispatcher.subscribeOnPersonalTopic('game/turn', (c) => {
        this.game.next(JSON.parse(c));
    });
    this.dispatcher.subscribeOnPersonalTopic('game/current', (c) => {
      console.log('WERKT: ', c);
      this.game.next(JSON.parse(c));
    });
  }

  public sendGameData(gameId: number, cardId: number, userId: number, uno: boolean) {
    const turn = new Turn();
    turn.gameId = gameId;
    turn.cardId = cardId;
    turn.userId = userId;
    turn.uno = uno;
    this.dispatcher.sendMessage('game/turn', turn);
  }

  public sendGameDataWithColor(gameId: number, cardId: number, userId: number, uno: boolean, color: string) {
    const turn = new Turn();
    turn.gameId = gameId;
    turn.cardId = cardId;
    turn.userId = userId;
    turn.uno = uno;
    turn.choiceColor = color;
    this.dispatcher.sendMessage('game/turn', turn);
  }

  public initializeGame() {
    this.dispatcher.sendMessage('game/get', {});
  }

  public getGameObservable() {
    return this.game.asObservable();
  }

  public destroyGame() {
    this.game = new Subject<Game>();
  }
}
