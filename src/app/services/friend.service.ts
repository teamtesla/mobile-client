import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { Subject } from 'rxjs';
import { WebsocketDispatcherService } from './websocket-dispatcher.service';
import { NotificationService, NotificationSort } from './notification.service';

@Injectable({
    providedIn: 'root'
})
export class FriendService {

    lastFriends: User[] = new Array();
    friends: Subject<User[]> = new Subject();
    friendRequests: FriendRequest[] = new Array();
    constructor(private dispatcher: WebsocketDispatcherService, private notifier: NotificationService) {
        this.doSubscriptions();
        this.getFriendRequests();
    }
    doSubscriptions() {
        var _this = this;
        this.dispatcher.subscribeOnPersonalTopic("friend/list", (x) => {
            var fl: FriendList = JSON.parse(x);
            _this.lastFriends = fl.friendlist;
            _this.friends.next(fl.friendlist);
        });

        this.dispatcher.subscribeOnPersonalTopic('friend/request/list', (x) => {
            const fRL: FrienRequestList = JSON.parse(x);
            _this.friendRequests = fRL.friendRequests;
        });

        this.dispatcher.subscribeOnPersonalTopic("friend/request/new", (x) => {
            var friendRequest: FriendRequest = JSON.parse(x);
            _this.friendRequests.push(friendRequest);
            _this.notifier.showNotification("You've got a friend invite from " + friendRequest.sender, NotificationSort.notify);
        });
    }

    sendFriendRequest(user: User) {
        var request : FriendRequest = new FriendRequest();
        request.receiver = user.email;
        this.dispatcher.sendMessage("friend/request/make", request);
    }
    acceptFriendRequest(friendRequest: FriendRequest) {
        this.friendRequests.slice(this.friendRequests.indexOf(friendRequest), 1);
        this.dispatcher.sendMessage('friend/request/accept', friendRequest);
    }
    getFriendRequests() {
        this.dispatcher.sendMessage('friend/request/list', new FrienRequestList());
    }
}

class FriendList {
    friendlist: User[];
}
class FriendRequest {
    sender: string;
    receiver: string;
    friendRequestId: number;
}

class FrienRequestList {
    friendRequests: FriendRequest[];
}
