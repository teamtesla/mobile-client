import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuardService as AuthGuard} from './services/auth-guard.service';

const routes: Routes = [
    {
        path: '',
        loadChildren: './pages/lobbies/lobbies.module#LobbiesPageModule'
    },
    {path: 'login', loadChildren: './pages/login/login.module#LoginPageModule'},
    {path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule'},
    {path: 'lobbies', loadChildren: './pages/lobbies/lobbies.module#LobbiesPageModule', canActivate: [AuthGuard]},
    {path: 'lobby', loadChildren: './pages/lobby/lobby.module#LobbyPageModule', canActivate: [AuthGuard]},
    {path: 'game', loadChildren: './pages/game/game.module#GamePageModule', canActivate: [AuthGuard]},
    {path: 'notifications', loadChildren: './pages/notifications/notifications.module#NotificationsPageModule', canActivate: [AuthGuard]},
    {path: 'statistics', loadChildren: './pages/statistics/statistics.module#StatisticsPageModule', canActivate: [AuthGuard]},
    {
        path: 'account',
        loadChildren: './pages/account/account.module#AccountPageModule'
    },
    {
        path: 'personallobbies',
        loadChildren: './pages/personallobbies/personallobbies.module#PersonallobbiesPageModule'
    }



    /*
      { path: 'account', loadChildren: './account/account.module#AccountPageModule' },
      { path: 'chatwindow', loadChildren: './chatwindow/chatwindow.module#ChatwindowPageModule' },
      { path: 'invite', loadChildren: './invite/invite.module#InvitePageModule' },
      { path: 'lobbies', loadChildren: './lobbies/lobbies.module#LobbiesPageModule' },
      { path: 'lobby', loadChildren: './lobby/lobby.module#LobbyPageModule' },
      { path: 'personallobbies', loadChildren: './personallobbies/personallobbies.module#PersonallobbiesPageModule' },
      { path: 'statistics', loadChildren: './statistics/statistics.module#StatisticsPageModule' },
    */


];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
