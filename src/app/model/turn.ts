export class Turn {
  gameId: number;
  cardId: number;
  userId: number;
  uno: boolean;
  choiceColor: string;
}
