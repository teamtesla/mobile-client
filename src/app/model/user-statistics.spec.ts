import {UserStatistics} from './user-statistics';
import {User} from './user';

describe('UserStatistics', () => {
  it('should create an instance', () => {
    expect(new UserStatistics(new User('testUser', 'testUsername', 'testPassword', 'test.mail@fake.mail'))).toBeTruthy();
  });
});
