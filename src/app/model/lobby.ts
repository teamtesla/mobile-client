import {User} from './user';

export class Lobby {
  lobbyId: number;
  name:  string;
  host: User;
  unoUsers: User[];
  playerCount: number;
  uuid: string;
  isHost: boolean;
  deleted: boolean;
  isPrivate: boolean;


  constructor(name: string) {
    this.name = name;
    this.unoUsers = new Array();
  }
}
